package coop.tecso.examen.constants;

public class Constants {
	
	//Currency Codes
	public static final String COP_CODE = "COP";
	public static final String USD_CODE = "USD";
	public static final String EUR_CODE = "EUR";
	
	//Movement types
	public static final String DEBIT_TYPE = "Debito";
	public static final String CREDIT_TYPE = "Credito";
	
	//Exceptions
	public static final String NOT_FUNDS_EXCEPTION = "La cuenta no posee fondos para realizar esta transacción";
	public static final String NOT_EMPTY_MOVEMENTS_EXCEPTION = "La cuenta tiene movimientos asociados y por ello no puede ser eliminada";
	public static final String NOT_FOUND_EXCEPTION = "No se han encontrado resultados por: ";
	public static final String UNIQUE_COLUMN_EXCEPTION = "El rut ingresado no es único";
}
