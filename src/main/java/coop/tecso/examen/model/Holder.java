package coop.tecso.examen.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "holder")
public class Holder extends AbstractPersistentObject {

	private static final long serialVersionUID = -3544481532948883691L;

	@Column(name = "rut")
	private String rut;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "document_id")
	private String identification;
	
	@Column(name = "foundationDate")
	private Date foundationDate;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "type_person")
	private String typePerson;

	public String getRut() {
		return rut;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getIdentification() {
		return identification;
	}

	public Date getFoundationDate() {
		return foundationDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public void setFoundationDate(Date foundationDate) {
		this.foundationDate = foundationDate;
	}

	public String getAddress() {
		return address;
	}

	public String getTypePerson() {
		return typePerson;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setTypePerson(String typePerson) {
		this.typePerson = typePerson;
	}

}
