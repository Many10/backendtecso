package coop.tecso.examen.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "movement")
public class Movement extends AbstractPersistentObject {

	private static final long serialVersionUID = -7383103476452115964L;

	@Column(name = "transaction_date")
	private Date transactionDate;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "amount")
	private double value;
	
	@ManyToOne
	@JoinColumn(name = "id_account", referencedColumnName = "id")
	private Account account;

	public Date getTransactionDate() {
		return transactionDate;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public double getValue() {
		return value;
	}

	public Account getAccount() {
		return account;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
