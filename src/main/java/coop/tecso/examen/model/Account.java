package coop.tecso.examen.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account extends AbstractPersistentObject {

	private static final long serialVersionUID = -4242127173069547694L;

	@Column(name = "account_number")
	private String accountNumber;
	
	@Column(name = "balance")
	private double balance;
	
	@Column(name = "currency")
	private String currency;
	
	@OneToOne
	@JoinColumn(name = "id_holder")
	private Holder holder;

	public String getAccountNumber() {
		return accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public String getCurrency() {
		return currency;
	}

	public Holder getHolder() {
		return holder;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public void setHolder(Holder holder) {
		this.holder = holder;
	}

}
