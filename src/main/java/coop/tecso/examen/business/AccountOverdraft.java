package coop.tecso.examen.business;

public interface AccountOverdraft {

	public boolean validateOverdraft(double balance, double amount);
	
}
