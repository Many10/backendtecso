package coop.tecso.examen.business.impl;

import coop.tecso.examen.business.AccountOverdraft;

public class AccountOverdraftEur implements AccountOverdraft{

	@Override
	public boolean validateOverdraft(double balance, double amount) {
		double overdraft = (balance - amount) * -1;
		if (overdraft < 150) {
			return true;
		}
		return false;
	}

}
