package coop.tecso.examen.business.impl;

import coop.tecso.examen.business.AccountOverdraft;

public class AccountOverdraftUsd implements AccountOverdraft{

	@Override
	public boolean validateOverdraft(double balance, double amount) {
		double overdraft = (balance - amount) * -1;
		if (overdraft < 300) {
			return true;
		}
		return false;
	}

}
