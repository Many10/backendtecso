package coop.tecso.examen.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import coop.tecso.examen.business.AccountOverdraft;
import coop.tecso.examen.constants.Constants;
import coop.tecso.examen.dto.AccountDto;
import coop.tecso.examen.dto.MovementDto;
import coop.tecso.examen.factory.OverdraftFactory;
import coop.tecso.examen.service.AccountService;
import coop.tecso.examen.service.MovementService;

@Component
public class MovementFacade {
	
	private final AccountService accountService;
	
	@Autowired
	public MovementFacade(AccountService accountService) {
		this.accountService = accountService;
	}

	public boolean validateOverdraft(long accountId, double amount) {
		AccountDto account = accountService.findAccount(accountId);
		AccountOverdraft accountOverdraft = OverdraftFactory.build(account.getCurrency());
		return accountOverdraft.validateOverdraft(account.getBalance(), amount);
	}
	
	public void updateAccountBalance(String transactionType, double amount, long accountId) {
		if(Constants.DEBIT_TYPE.equals(transactionType)) {
			AccountDto account = accountService.findAccount(accountId);
			double newBalance = account.getBalance() - amount;
			account.setBalance(newBalance);
			accountService.updateAccount(accountId, account);
		}
	}

}
