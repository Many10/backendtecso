package coop.tecso.examen.factory;

import org.springframework.stereotype.Component;

import coop.tecso.examen.business.AccountOverdraft;
import coop.tecso.examen.business.impl.AccountOverdraftCop;
import coop.tecso.examen.business.impl.AccountOverdraftEur;
import coop.tecso.examen.business.impl.AccountOverdraftUsd;
import coop.tecso.examen.constants.Constants;

@Component
public class OverdraftFactory {

	public static AccountOverdraft build(String parameter) {
		AccountOverdraft result = null;
		switch(parameter) {
			case Constants.COP_CODE: 
				result = new AccountOverdraftCop();
				break;
			case Constants.USD_CODE: 
				result = new AccountOverdraftUsd();
				break;
			case Constants.EUR_CODE: 
				result = new AccountOverdraftEur();
				break;
		}
		return result;
	}

}
