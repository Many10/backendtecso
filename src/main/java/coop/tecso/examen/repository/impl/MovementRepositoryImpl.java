package coop.tecso.examen.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import coop.tecso.examen.model.Movement;
import coop.tecso.examen.repository.MovementRepositoryCustom;

public class MovementRepositoryImpl implements MovementRepositoryCustom {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Movement> findMovementByAccount(long idAccount) {
		TypedQuery<Movement> query = em.createQuery("FROM Movement m WHERE m.account.id = :account ", Movement.class);
		query.setParameter("account", idAccount);
		return query.getResultList();
	}

}
