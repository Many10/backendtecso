package coop.tecso.examen.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import coop.tecso.examen.model.Holder;
import coop.tecso.examen.repository.HolderRepositoryCustom;

@Repository
public class HolderRepositoryImpl implements HolderRepositoryCustom{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Holder findByRut(String rut) {
		Query query = em.createNativeQuery("FROM Holder h WHERE h.rut = :rut ", Holder.class);
		query.setParameter("rut", rut);
		return (Holder) query.getSingleResult();
	}

}
