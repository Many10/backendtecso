package coop.tecso.examen.repository;

import coop.tecso.examen.model.Holder;

public interface HolderRepositoryCustom {

	public Holder findByRut(String rut);
}
