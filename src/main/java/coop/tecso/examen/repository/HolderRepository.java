 package coop.tecso.examen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coop.tecso.examen.dto.HolderDto;
import coop.tecso.examen.model.Holder;

@Repository
public interface HolderRepository extends JpaRepository<Holder, Long>, HolderRepositoryCustom{

	
}
