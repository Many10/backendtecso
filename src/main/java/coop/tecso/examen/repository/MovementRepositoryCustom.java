package coop.tecso.examen.repository;

import java.util.List;

import coop.tecso.examen.model.Movement;

public interface MovementRepositoryCustom {

	public List<Movement> findMovementByAccount(long idAccount);
	
}
