package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.HolderDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.service.HolderService;

@RequestMapping("/holder")
@RestController
public class HolderController {
	
	private final HolderService holderService;
	
	@Autowired
	public HolderController(HolderService holderService) {
		this.holderService = holderService;
	}
	
	@PostMapping
	public HolderDto addHolder(@RequestBody HolderDto holder) throws AppException{
		return holderService.addHolder(holder);
	}
	
	@GetMapping(path = "{id}")
	public HolderDto findHolderById(@PathVariable("id") long id) {
		return holderService.findHolder(id);
	}
	
	@GetMapping
	public List<HolderDto> getAll() {
		return holderService.findAll();
	}
	
	@GetMapping(path = "/rut/{rut}")
	public HolderDto findHolderByRut(String rut) throws AppException {
		return holderService.findHolderByRut(rut);
	}
	
	@PutMapping(path = "{id}")
	public HolderDto updateHolder(@PathVariable("id") long id, @RequestBody HolderDto holder) {
		return holderService.updateHolder(id, holder);
	}
	
	@DeleteMapping(path = "{id}")
	public void deleteHolder(@PathVariable("id") long id) {
		holderService.deleteHolder(id);
	}
	
}
