package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.MovementDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.service.MovementService;

@RequestMapping("/movement")
@RestController
public class MovementController {

	private final MovementService movementService;
	
	@Autowired
	public MovementController(MovementService movementService) {
		this.movementService = movementService;
	}
	
	@PostMapping
	public MovementDto addMovement(@RequestBody MovementDto movement) throws AppException {
		return movementService.addMovement(movement);
	}
	
	@GetMapping(path = "{id}")
	public MovementDto getMovement(@PathVariable long id) {
		return movementService.findMovement(id);
	}
	
	@GetMapping
	public List<MovementDto> getAll() {
		return movementService.findAll();
	}
	
	@GetMapping(path = "/account/{accountId}")
	public List<MovementDto> getByAccountId(@PathVariable long accountId) {
		return movementService.findByAccountId(accountId);
	}
	
}
