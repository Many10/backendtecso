package coop.tecso.examen.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.AccountDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.service.AccountService;

@RequestMapping("/account")
@RestController
public class AccountController {

	private final AccountService accountService;
	
	@Autowired
	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@PostMapping
	public AccountDto addAccount(@RequestBody AccountDto account) {
		return accountService.addAccount(account);
	}
	
	@GetMapping(path = "{id}")
	public AccountDto findAccountById(@PathVariable("id") long id) {
		return accountService.findAccount(id);
	}
	
	@GetMapping
	public List<AccountDto> getAll() {
		return accountService.findAll();
	}
	
	@PutMapping(path = "{id}")
	public AccountDto updateAccount(@PathVariable("id") long id, @RequestBody AccountDto holder) {
		return accountService.updateAccount(id, holder);
	}
	
	@DeleteMapping(path = "{id}")
	public void deleteAccount(@PathVariable("id") long id) throws AppException{
		accountService.deleteAccount(id);
	}

}
