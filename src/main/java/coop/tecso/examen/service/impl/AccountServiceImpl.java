package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.constants.Constants;
import coop.tecso.examen.dto.AccountDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.mapper.AccountMapper;
import coop.tecso.examen.model.Account;
import coop.tecso.examen.repository.AccountRepository;
import coop.tecso.examen.repository.MovementRepository;
import coop.tecso.examen.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	private final AccountRepository accountRepository;
	private final MovementRepository movementRepository;
	
	@Autowired
	public AccountServiceImpl(AccountRepository accountRepository, MovementRepository movementRepository) {
		this.accountRepository = accountRepository;
		this.movementRepository = movementRepository;
	}
	
	@Override
	public AccountDto addAccount(AccountDto account) {
		Account entity = AccountMapper.createEntityFromDto(account);
		AccountDto result = null;
		if (entity != null) {
			result = AccountMapper.createDtoFromEntity(accountRepository.save(entity));
		}
		return result;
	}

	@Override
	public AccountDto findAccount(long id) {
		Account entity = accountRepository.findById(id).orElse(null);
		if (entity != null) {
			AccountDto account = AccountMapper.createDtoFromEntity(entity);
			return account;
		}
		return null;
	}

	@Override
	public List<AccountDto> findAll() {
		List<AccountDto> accounts = AccountMapper.createListDtoFromListEntity(accountRepository.findAll());
		return accounts;
	}

	@Override
	public AccountDto updateAccount(long id, AccountDto account) {
		Account entity = AccountMapper.createEntityFromDto(account);
		Account oldAccount = AccountMapper.createEntityFromDto(this.findAccount(id));
		if(oldAccount != null) {
			entity.setId(oldAccount.getId());
			entity.setVersionNumber(oldAccount.getVersionNumber());
			return AccountMapper.createDtoFromEntity(accountRepository.save(entity));
		}
		return null;
	}

	@Override
	public void deleteAccount(long id) throws AppException{
		AccountDto deleteAccount = this.findAccount(id);
		if (movementRepository.findMovementByAccount(id).isEmpty()) {
			accountRepository.delete(AccountMapper.createEntityFromDto(deleteAccount));
		} else {
			throw new AppException(Constants.NOT_EMPTY_MOVEMENTS_EXCEPTION, new Exception());
		}
	}

	
	
}
