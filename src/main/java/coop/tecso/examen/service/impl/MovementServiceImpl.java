package coop.tecso.examen.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.constants.Constants;
import coop.tecso.examen.dto.MovementDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.facade.MovementFacade;
import coop.tecso.examen.mapper.MovementMapper;
import coop.tecso.examen.model.Movement;
import coop.tecso.examen.repository.MovementRepository;
import coop.tecso.examen.service.MovementService;

@Service
public class MovementServiceImpl implements MovementService {

	private final MovementRepository movementRepository;
	private final MovementFacade facade;
	
	@Autowired
	public MovementServiceImpl(MovementRepository movementRepository, MovementFacade facade) {
		this.movementRepository = movementRepository;
		this.facade = facade;
	}
	
	@Override
	public MovementDto addMovement(MovementDto movement) throws AppException{
		Movement entity = MovementMapper.createEntityFromDto(movement);
		MovementDto result = null;
		if (entity != null) {
			if (facade.validateOverdraft(entity.getAccount().getId(), entity.getValue())) {
				result = MovementMapper.createDtoFromEntity(movementRepository.save(entity));
				facade.updateAccountBalance(entity.getType(), entity.getValue(), entity.getAccount().getId());
			} else {
				throw new AppException(Constants.NOT_FUNDS_EXCEPTION, new Exception());
			}
		}
		return result;
	}

	@Override
	public MovementDto findMovement(long id) {
		Movement entity = movementRepository.findById(id).orElse(null);
		if (entity != null) {
			MovementDto movement = MovementMapper.createDtoFromEntity(entity);
			return movement;
		}
		return null;
	}

	@Override
	public List<MovementDto> findAll() {
		List<MovementDto> movements = MovementMapper.createListDtoFromListEntity(movementRepository.findAll());
		return movements;
	}

	@Override
	public List<MovementDto> findByAccountId(long accountId) {
		List<MovementDto> movements = MovementMapper.createListDtoFromListEntity(movementRepository.findMovementByAccount(accountId));
		return movements;
	}

	
	
}
