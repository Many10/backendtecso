package coop.tecso.examen.service.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import coop.tecso.examen.constants.Constants;
import coop.tecso.examen.dto.HolderDto;
import coop.tecso.examen.exceptions.AppException;
import coop.tecso.examen.mapper.HolderMapper;
import coop.tecso.examen.model.Holder;
import coop.tecso.examen.repository.HolderRepository;
import coop.tecso.examen.service.HolderService;

@Service
public class HolderServiceImpl implements HolderService {

	private final HolderRepository holderRepository;

	@Autowired
	public HolderServiceImpl(HolderRepository holderRepository) {
		this.holderRepository = holderRepository;
	}

	@Override
	public HolderDto addHolder(HolderDto holder) throws AppException{
		final Holder entity = HolderMapper.createEntityFromDto(holder);
		HolderDto holderSaved = null;
		try {
			if (entity != null) {
				holderSaved = HolderMapper.createDtoFromEntity(holderRepository.save(entity));
			}
		} catch (ConstraintViolationException cex) {
			throw new AppException(Constants.UNIQUE_COLUMN_EXCEPTION, new Exception());
		}
		return holderSaved;
	}

	@Override
	public HolderDto findHolder(long id) {
		Holder entity = holderRepository.findById(id).orElse(null);
		if (entity != null) {
			HolderDto holder = HolderMapper.createDtoFromEntity(entity);
			return holder;
		}
		return null;
	}

	@Override
	public List<HolderDto> findAll() {
		List<HolderDto> listHolder = HolderMapper.ListEntityToListDto(holderRepository.findAll());
		return listHolder;
	}

	@Override
	public HolderDto updateHolder(long id, HolderDto holder) {
		Holder entity = HolderMapper.createEntityFromDto(holder);
		Holder oldHolder = holderRepository.findById(id).orElse(null);
		if (oldHolder != null) {
			entity.setId(oldHolder.getId());
			entity.setVersionNumber(oldHolder.getVersionNumber());
			return HolderMapper.createDtoFromEntity(holderRepository.save(entity));
		}
		return null;
	}

	@Override
	public void deleteHolder(long id) {
		HolderDto deleteHolder = this.findHolder(id);
		holderRepository.delete(HolderMapper.createEntityFromDto(deleteHolder));
	}

	@Override
	public HolderDto findHolderByRut(String rut) throws AppException {
		try {
			Holder entity = holderRepository.findByRut(rut);
			if (entity != null) {
				HolderDto holder = HolderMapper.createDtoFromEntity(entity);
				return holder;
			}
		} catch (NoResultException nex) {
			throw new AppException(Constants.NOT_FOUND_EXCEPTION + rut, new Exception());
		}
		return null;
	}

}
