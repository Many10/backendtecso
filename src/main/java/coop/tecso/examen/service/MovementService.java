package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.dto.MovementDto;
import coop.tecso.examen.exceptions.AppException;

public interface MovementService {

	public MovementDto addMovement(MovementDto movement) throws AppException;
	
	public MovementDto findMovement(long id);
	
	public List<MovementDto> findAll();
	
	public List<MovementDto> findByAccountId(long accountId);
	
}
