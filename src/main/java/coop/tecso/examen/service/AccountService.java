package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.dto.AccountDto;
import coop.tecso.examen.exceptions.AppException;

public interface AccountService {

	public AccountDto addAccount(AccountDto account);
	
	public AccountDto findAccount(long id);
	
	public List<AccountDto> findAll();
	
	public AccountDto updateAccount(long id, AccountDto account);
	
	public void deleteAccount(long id) throws AppException;
}
