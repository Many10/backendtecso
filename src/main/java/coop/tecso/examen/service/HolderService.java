package coop.tecso.examen.service;

import java.util.List;

import coop.tecso.examen.dto.HolderDto;
import coop.tecso.examen.exceptions.AppException;

public interface HolderService {

	public HolderDto addHolder(HolderDto holder) throws AppException;
	
	public HolderDto findHolder(long id);
	
	public List<HolderDto> findAll();
	
	public HolderDto updateHolder(long id, HolderDto holder);
	
	public void deleteHolder(long id);
	
	public HolderDto findHolderByRut(String rut) throws AppException;
	
}
