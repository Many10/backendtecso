package coop.tecso.examen.exceptions;

public class AppException extends Exception {

	private static final long serialVersionUID = 2884752743559448878L;

	public AppException(String cause, Exception e) {
		super(cause, e);
	}
}
