package coop.tecso.examen.mapper;

import java.util.ArrayList;
import java.util.List;

import coop.tecso.examen.dto.HolderDto;
import coop.tecso.examen.model.Holder;

public class HolderMapper {

	public static Holder createEntityFromDto(HolderDto holder) {
		final Holder entity = new Holder();
		if (holder != null) {
			entity.setAddress(holder.getAddress());
			entity.setId(holder.getId());
			entity.setEmail(holder.getEmail());
			entity.setPhone(holder.getPhone());
			entity.setRut(holder.getRut());
			entity.setTypePerson(holder.getTypePerson());
			entity.setFoundationDate(holder.getFoundationDate());
			entity.setIdentification(holder.getIdentification());
			entity.setName(holder.getName());
			entity.setLastName(holder.getLastName());
		}
		return entity;
	}
	
	public static HolderDto createDtoFromEntity(Holder holder) {
		final HolderDto dto = new HolderDto();
		if (holder != null) {
			dto.setAddress(holder.getAddress());
			dto.setId(holder.getId());
			dto.setEmail(holder.getEmail());
			dto.setPhone(holder.getPhone());
			dto.setRut(holder.getRut());
			dto.setTypePerson(holder.getTypePerson());
			dto.setFoundationDate(holder.getFoundationDate());
			dto.setIdentification(holder.getIdentification());
			dto.setName(holder.getName());
			dto.setLastName(holder.getLastName());
		}
		return dto;
	}
	
	public static List<HolderDto> ListEntityToListDto(List<Holder> listHolder) {
		final List<HolderDto> result = new ArrayList<HolderDto>();
		if (listHolder != null) {
			for(Holder entity: listHolder) {
				result.add(createDtoFromEntity(entity));
			}
		}
		return result;
	}
}
