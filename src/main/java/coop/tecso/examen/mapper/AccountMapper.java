package coop.tecso.examen.mapper;

import java.util.ArrayList;
import java.util.List;

import coop.tecso.examen.dto.AccountDto;
import coop.tecso.examen.model.Account;

public class AccountMapper {
	
	public static AccountDto createDtoFromEntity(Account entity) {
		AccountDto account = new AccountDto();
		if (entity != null) {
			account.setAccountNumber(entity.getAccountNumber());
			account.setBalance(entity.getBalance());
			account.setCurrency(entity.getCurrency());
			account.setHolder(HolderMapper.createDtoFromEntity(entity.getHolder()));
			account.setId(entity.getId());
		}
		return account;
	}
	
	public static Account createEntityFromDto(AccountDto dto) {
		Account account = new Account();
		if (dto != null) {
			account.setAccountNumber(dto.getAccountNumber());
			account.setBalance(dto.getBalance());
			account.setCurrency(dto.getCurrency());
			account.setHolder(HolderMapper.createEntityFromDto(dto.getHolder()));
			account.setId(dto.getId());
		}
		return account;
	}
	
	public static List<AccountDto> createListDtoFromListEntity(List<Account> list) {
		List<AccountDto> accounts = new ArrayList<AccountDto>();
		if (list != null) {
			for (Account entity : list) {
				accounts.add(createDtoFromEntity(entity));
			}
		}
		return accounts;
	}

}
