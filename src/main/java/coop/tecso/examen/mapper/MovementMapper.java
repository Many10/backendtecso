package coop.tecso.examen.mapper;

import java.util.ArrayList;
import java.util.List;

import coop.tecso.examen.dto.MovementDto;
import coop.tecso.examen.model.Movement;

public class MovementMapper {

	public static MovementDto createDtoFromEntity(Movement entity) {
		MovementDto movement = new MovementDto();
		if (entity != null) {
			movement.setId(entity.getId());
			movement.setTransactionDate(entity.getTransactionDate());
			movement.setType(entity.getType());
			movement.setAmount(entity.getValue());
			movement.setDescription(entity.getDescription());
			movement.setAccount(AccountMapper.createDtoFromEntity(entity.getAccount()));
		}
		return movement;
	}
	
	public static Movement createEntityFromDto(MovementDto dto) {
		Movement movement = new Movement();
		if (dto != null) {
			movement.setId(dto.getId());
			movement.setTransactionDate(dto.getTransactionDate());
			movement.setType(dto.getType());
			movement.setValue(dto.getAmount());
			movement.setDescription(dto.getDescription());
			movement.setAccount(AccountMapper.createEntityFromDto(dto.getAccount()));
		}
		return movement;
	}
	
	public static List<MovementDto> createListDtoFromListEntity(List<Movement> list) {
		final List<MovementDto> result = new ArrayList<MovementDto>();
		if (list != null) {
			for (Movement entity : list) {
				result.add(createDtoFromEntity(entity));
			}
		}
		return result;
	}
	
}
