package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.Date;

import coop.tecso.examen.model.Account;

public class MovementDto implements Serializable {
	
	private static final long serialVersionUID = 1797437264938660339L;

	private long id;
	private Date transactionDate;
	private String type;
	private String description;
	private double amount;
	private AccountDto account;

	public long getId() {
		return id;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public double getAmount() {
		return amount;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public AccountDto getAccount() {
		return account;
	}

	public void setAccount(AccountDto account) {
		this.account = account;
	}

}
