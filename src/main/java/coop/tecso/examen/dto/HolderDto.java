package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.Date;

public class HolderDto implements Serializable {

	private static final long serialVersionUID = -1016195908224784257L;

	private long id;
	private String rut;
	private String name;
	private String lastName;
	private String identification;
	private Date foundationDate;
	private String phone;
	private String address;
	private String email;
	private String typePerson;

	public String getTypePerson() {
		return typePerson;
	}

	public void setTypePerson(String typePerson) {
		this.typePerson = typePerson;
	}

	public String getRut() {
		return rut;
	}

	public String getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public String getEmail() {
		return email;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getIdentification() {
		return identification;
	}

	public Date getFoundationDate() {
		return foundationDate;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public void setFoundationDate(Date foundationDate) {
		this.foundationDate = foundationDate;
	}
	
}
