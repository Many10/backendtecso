package coop.tecso.examen.dto;

import java.io.Serializable;
import java.util.List;

public class AccountDto implements Serializable {

	private static final long serialVersionUID = 7653168659400121374L;

	private long id;
	private String accountNumber;
	private double balance;
	private String currency;
	private HolderDto holder;

	public long getId() {
		return id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public double getBalance() {
		return balance;
	}

	public String getCurrency() {
		return currency;
	}

	public HolderDto getHolder() {
		return holder;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setHolder(HolderDto holder) {
		this.holder = holder;
	}

}
