### SOBRE LA PRUEBA ###

1. Ejercicio: Se desarrolla el CRUD para titulares de cuentas. Se abstrae la información entre persona natural y persona jurídica con el fin de compartir las mismas estructuras. Se desarrolla el front-end en un proyecto angular aparte.

2. Ejercicio: Se desarrolla el negocio para el manejo de cuentas y movimientos en cuentas. Las url de los servicios se pueden ver en http://localhost:8080/api/swagger-ui.html. Se utiliza swagger como landing page ya que permite visualizar un blueprint de los servicios.

### BONUS ###

1. Se incorpora el componente swagger-ui con el fin de ofrecer una visualización tipo landing page de los servicios de la aplicación.
2. Se desarrolla base de datos en SQL Server. Debe modificarse el usuario, password y url de la base de datos para hacer despliegue en otro servidor.

Se adjuntan los componentes en el repositorio.